use std::io::prelude::*;
use std::net::Shutdown;
use std::os::unix::net::{UnixListener, UnixStream};
use std::path::Path;
use std::thread;
use std::time::Duration;

use chrono::{DateTime, Local};
use clap::ArgMatches;
use cli::cli;
use errors::*;
use file;
use nix::sys::signal::{self, kill, SigSet, Signal};
use nix::sys::signalfd::SignalFd;
use nix::unistd;
use notify_rust::Notification;

static SOCKET_PATH: &'static str = "/tmp/jot";

#[derive(Debug)]
enum DaemonSignal {
    Stop,
    Restart,
    Status,
}

pub fn handler(jp: &Path, m: &ArgMatches<'static>) -> Result<()> {
    match m.subcommand() {
        ("start", Some(subs)) => run_loop(jp, subs),
        ("stop", Some(subs)) => send_ipc(&DaemonSignal::Stop, subs),
        ("restart", Some(subs)) => send_ipc(&DaemonSignal::Restart, subs),
        ("status", Some(subs)) => send_ipc(&DaemonSignal::Status, subs),
        _ => cli().print_help().chain_err(|| "unable to print help"),
    }
}

fn run_loop(jp: &Path, m: &ArgMatches<'static>) -> Result<()> {
    info!("daemon starting");
    let mut children = vec![];

    // signal handling; SIGINT, SIGUSR1 (exit), SIGUSR2 (restart)
    let mut mask = SigSet::empty();
    mask.add(signal::SIGINT);
    mask.add(signal::SIGUSR1);
    mask.add(signal::SIGUSR2);
    mask.thread_block()?;
    children.push(thread::spawn(
        move || sig_handler(&mut SignalFd::new(&mask).unwrap()),
    ));
    children.push(thread::spawn(move || control_handler()));

    loop {
        let _jots = file::read_from(jp)?.entry;
        let jots: Vec<_> = _jots.iter().filter(|j| !j.due.is_none()).collect();
        debug!("loaded jots");

        for j in &jots {
            // expensive process of converting between `toml` and `chrono`
            let date = j.due.clone().unwrap();
            let today = Local::today();
            let jot_date = DateTime::parse_from_rfc3339(
                format!("{}T00:00:00-00:00", date).as_str(),
            )?.with_timezone(&(today.timezone()))
                .date();
            if jot_date == today.succ() {
                info!("{} due tomorrow", j);
                Notification::new()
                    .summary("jot: due tomorrow")
                    .body(&format!("{}", j))
                    .show()
                    .unwrap();
            }
        }
        thread::sleep(Duration::new(3600 * 12, 0));
    }

    Ok(())
}

fn control_handler() -> Result<()> {
    debug!("launching control handler");
    let listener = UnixListener::bind(Path::new(SOCKET_PATH))
        .chain_err(|| "unable to bind to socket")?;
    debug!("started listening with PID: {}", unistd::getpid());
    for stream in listener.incoming() {
        match stream {
            Ok(s) => {
                debug!("connection recieved");
                thread::spawn(move || response_handler(&s))
            }
            Err(_) => break,
        };
    }
    Ok(())
}

fn handle_sigint() -> Result<()> {
    // emergency cleanup
    ::std::fs::remove_file(Path::new(SOCKET_PATH))?;
    kill(unistd::getpgrp(), None)?;
    Ok(())
}

fn handle_sigusr1() -> Result<()> {
    // safe cleanup
    ::std::fs::remove_file(Path::new(SOCKET_PATH))?;
    kill(unistd::getpgrp(), Signal::SIGINT)?;
    Ok(())
}

fn handle_sigusr2() -> Result<()> {
    // restart
    ::std::fs::remove_file(Path::new(SOCKET_PATH))?;
    kill(unistd::getpgrp(), None)?;
    Ok(())
}

fn sig_handler(fd: &mut SignalFd) -> Result<()> {
    debug!("launching signal handler");
    let res = fd.read_signal()?.unwrap();
    let signo = Signal::from_c_int(res.ssi_signo as i32)?;
    debug!("{:?} received", signo);

    match signo {
        Signal::SIGINT => handle_sigint(),
        Signal::SIGUSR1 => handle_sigusr1(),
        Signal::SIGUSR2 => handle_sigusr2(),
        _ => {
            debug!("unhandled signal {:?}", signo);
            Ok(())
        }
    }
}

fn response_handler(stream: &UnixStream) -> Result<()> {
    let mut resp = String::new();
    let mut clone = stream.try_clone()?;
    debug!("cloned socket for manipulation");
    clone
        .read_to_string(&mut resp)
        .chain_err(|| "unable to read from socket")?;
    debug!("read message from socket");
    match resp.as_str() {
        "stop" => {
            debug!("\"stop\" received; shutting down socket");
            kill(unistd::getpid(), signal::SIGUSR1)?;
            Ok(())
        }
        "status" => {
            debug!("status requested");
            let status =
                format!("jot-daemon -- running with pid: {}", unistd::getpid());
            clone
                .write_all(status.as_bytes())
                .chain_err(|| "unable to write to socket")?;
            resp.clear();
            debug!("status sent");
            Ok(())
        }
        _ => Err(ErrorKind::SocketComm.into()),
    }
}

fn send_ipc(sign: &DaemonSignal, m: &ArgMatches<'static>) -> Result<()> {
    let mut sock = UnixStream::connect(Path::new(SOCKET_PATH))
        .chain_err(|| "unable to connect to socket")?;

    macro_rules! send_and_close {
        ($s:expr) => ({
            sock.write_all($s)
                .chain_err(|| "unable to write to socket")?;
            sock.shutdown(Shutdown::Write)
                .chain_err(|| "unable to close writing socket")?;
        });
    }

    debug!("requesting {:?}", sign);
    match *sign {
        DaemonSignal::Status => {
            debug!("fetching status");
            send_and_close!(b"status");
            let mut resp = String::new();
            debug!("waiting on response");
            sock.read_to_string(&mut resp)
                .chain_err(|| "unable to read from socket")?;
            println!("{}", resp);
        }
        DaemonSignal::Restart => unimplemented!(),
        DaemonSignal::Stop => {
            debug!("sending stop signal to daemon");
            send_and_close!(b"stop");
        }
    }
    Ok(())
}
