
extern crate chrono;
extern crate clap;
extern crate log;
extern crate nix;
extern crate regex;
extern crate toml;

use std;

error_chain! {
    foreign_links {
        Clap(clap::Error);
        Chrono(chrono::ParseError);
        Datetime(toml::value::DatetimeParseError);
        Env(std::env::VarError);
        Io(std::io::Error);
        Log(log::SetLoggerError);
        Regex(regex::Error);
        ParseNum(std::num::ParseIntError);
        TomlDe(toml::de::Error);
        TomlSer(toml::ser::Error);
        Nix(nix::Error);
    }
    errors {
        InvalidDate(content: String) {
            description("couldn't parse date")
            display("unable to parse date in content `{}`", content)
        }
        SocketComm {
            description("unrecognised ipc call")
            display("unrecognised ipc call")
        }
    }
}
