use clap::{App, Arg, SubCommand};

pub fn cli() -> App<'static, 'static> {
    App::new(crate_name!())
        .version(crate_version!())
        .author(crate_authors!())
        .about(crate_description!())
        .arg(
            Arg::with_name("config")
                .short("c")
                .long("config")
                .help("Config file to use")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("jotfile")
                .short("j")
                .long("jotfile")
                .help("Jotfile to use")
                .takes_value(true),
        )
        .arg(
            Arg::with_name("verbose")
                .short("v")
                .help("Set verbosity")
                .multiple(true)
        )
        .subcommand(
            SubCommand::with_name("add")
                .about("Creates a new jot")
                .arg(
                    Arg::with_name("extended")
                        .short("x")
                        .long("ext")
                        .help("Forces jot to be marked as extended"),
                )
                .arg(
                    Arg::with_name("msg")
                        .short("m")
                        .long("msg")
                        .help("Use <msg> as the jot content")
                        .takes_value(true),
                ),
        )
        .subcommand(
            SubCommand::with_name("complete")
                .alias("comp")
                .about("Marks a jot as being done")
                .arg(
                    Arg::with_name("number")
                        .help("Jot number to mark")
                        .required(true),
                ),
        )
        .subcommand(
            SubCommand::with_name("edit")
                .about("Edits a jot")
                .arg(
                    Arg::with_name("extended")
                        .short("x")
                        .long("ext")
                        .help("Use `$EDITOR` to edit"),
                )
                .arg(
                    Arg::with_name("msg")
                        .short("m")
                        .long("msg")
                        .help("Use <msg> as the jot content")
                        .takes_value(true),
                )
                .arg(
                    Arg::with_name("number")
                        .help("Jot number to mark")
                        .required(true),
                ),
        )
        .subcommand(
            SubCommand::with_name("list")
                .about("List all jots")
                .alias("ls")
                .arg(
                    Arg::with_name("all")
                        .short("a")
                        .long("all")
                        .help("Prints all jots"),
                )
                .arg(
                    Arg::with_name("done")
                        .short("d")
                        .long("done")
                        .help("Prints all jots marked done"),
                )
                .arg(
                    Arg::with_name("extended")
                        .short("x")
                        .long("ext")
                        .help("Expands all jots"),
                )
                .arg(Arg::with_name("number").help("Jot number to show")),
        )
        .subcommand(
            SubCommand::with_name("remove")
                .about("Remove a jot")
                .alias("rm")
                .arg(
                    Arg::with_name("number")
                        .help("Jot number to remove")
                        .required(true),
                ),
        )
        .subcommand(
            SubCommand::with_name("clean")
                .about("Removes all completed jots"),
        )
        .subcommand(
            SubCommand::with_name("daemon")
                .about("Control the reminder daemon")
                .subcommand(
                    SubCommand::with_name("start")
                        .about("Start the daemon")
                        .arg(
                            Arg::with_name("debug").help("Start in debug mode"),
                        ),
                )
                .subcommand(
                    SubCommand::with_name("stop").about("Stop the daemon"),
                )
                .subcommand(
                    SubCommand::with_name("restart")
                        .about("Restart the daemon"),
                )
                .subcommand(
                    SubCommand::with_name("status")
                        .about("View the current daemon status"),
                ),
        )
}
