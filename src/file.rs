use std::fmt;
use std::fs::{File, OpenOptions};
use std::io::{Read, Write};
use std::path::Path;
use std::str::FromStr;

use colored::Colorize;
use config::Config;
use errors::*;
use regex::Regex;
use toml;
use toml::value::Datetime;

#[derive(Deserialize, Serialize)]
pub struct Jotfile {
    pub entry: Vec<Entry>,
}

#[derive(Clone, Debug, Deserialize, PartialEq, Serialize)]
pub struct Entry {
    pub content: String,
    pub ext: bool,
    pub done: bool,
    pub due: Option<Datetime>,
}

impl Entry {
    pub fn new(c: &str) -> Result<Entry> {
        Ok(Entry {
            // TODO Strip date from content if available
            ext: c.contains('\n'),
            // content: c.to_owned(),
            content: strip_date(c)?,
            done: false,
            due: date_from_content(c)?,
        })
    }

    /// Formats an entry with a provided config's content parameters.
    /// Intended for use when a config might be different to the default
    /// (as `fmt::Display` cannot take additional arguments).
    pub fn display_with(&self, c: &Config, idx: usize, ex: bool) -> String {
        macro_rules! str {
            ($n:expr) => (&format!("{}", $n));
            ($n:expr, $w:expr) => (&format!("{:0w$}", $n, w = $w));
        }

        let (f, d) = c.formats();
        let fmt = if let Some(ref date) = self.due {
            debug!("using date formatting");
            d.replace("{i}", str!(idx, c.output.padding as usize))
                .replace("{c}", &self.content)
                .replace("{d}", str!(date))
        } else {
            debug!("using default formatting");
            f.replace("{i}", str!(idx, c.output.padding as usize))
                .replace("{c}", &self.content)
        };
        let _fmt = if self.ext {
            debug!("entry {} is extended", idx);
            if ex {
                debug!("forcing extended output");
                fmt.replace(
                    '\n',
                    &(String::from("\n") +
                        &" ".repeat(c.output.indent as usize)),
                )
            } else {
                format!("{} {}", fmt.split('\n').next().unwrap(), c.output.ext)
            }
        } else {
            fmt
        };
        if c.output.emph_title && _fmt.contains("::") {
            debug!("emphasising title...");
            let r = Regex::new(r#" ([\w ]*) ::"#).unwrap();
            let c = r.captures(_fmt.as_str()).unwrap();
            let title = c[1].to_owned();
            debug!("... found {}", title);
            let _split: Vec<_> = _fmt.split(title.as_str()).collect();
            format!(
                " {} {} {}",
                _split[0].trim(),
                title.bold(),
                _split[1].trim()
            )
        } else {
            format!(" {}", _fmt.trim())
        }
    }
}

impl fmt::Display for Entry {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        if self.ext {
            write!(f, "{} ...", self.content.split('\n').next().unwrap())
        } else {
            write!(f, "{}", self.content)
        }
    }
}

/// Finds a date from a string input. Returns Ok(Some(date)) on a good date,
/// Ok(None) on no date, or Err() on a malformed date. Will only return the
/// first capture.
fn date_from_content(con: &str) -> Result<Option<Datetime>> {
    if con.contains('@') {
        let r = Regex::new(r#"@(\d{4}-\d{2}-\d{2})"#)?;
        if let Some(cap) = r.captures(con) {
            debug!("found date in entry");
            Ok(Some(Datetime::from_str(&cap[1])?))
        } else {
            Err(ErrorKind::InvalidDate(con.to_owned()).into())
        }
    } else {
        Ok(None)
    }
}

/// Removes a `toml::Datetime` format date from a string. Returns the original
/// string if no date is present (wrapped in `Ok()`), the adjusted string if a
/// date is present, or `Err` on a malformed date.
fn strip_date(con: &str) -> Result<String> {
    if con.contains('@') {
        let r = Regex::new(r#"@\d{4}-\d{2}-\d{2}"#)?;
        if let Some(cap) = r.captures(con) {
            debug!("found date in entry");
            macro_rules! format_trim {
                ($s1:expr, $s2:expr) => (format!("{} {}", $s1, $s2)
                                        .replace("  ", " ")
                                        .replace("\n\n", "\n")
                                        .replace(" \n", "\n")
                )};

            let s: Vec<_> = con.split(&cap[0]).collect();
            if s[1].is_empty() {
                Ok(s[0].trim().to_owned())
            } else {
                debug!("trimming around date");
                Ok(format_trim!(s[0], s[1]))
            }
        } else {
            Err(ErrorKind::InvalidDate(con.to_owned()).into())
        }
    } else {
        Ok(String::from(con))
    }
}

/// Write a single `Entry` to the provided jotfile.
pub fn write_entry(path: &Path, e: Entry) -> Result<()> {
    let mut f = OpenOptions::new()
        .write(true)
        .create(true)
        .append(true)
        .open(path)?;
    f.write_all(
        toml::to_string_pretty(&Jotfile { entry: vec![e] })?
            .as_bytes(),
    )?;
    debug!("wrote single entry to {}", path.display());
    Ok(())
}

/// Rewrite a jotfile using the contents of a `Jotfile`.
pub fn write_all(path: &Path, j: &Jotfile) -> Result<()> {
    let mut f = OpenOptions::new()
        .write(true)
        .create(true)
        .truncate(true)
        .open(path)?;
    f.write_all(toml::to_string_pretty(&j)?.as_bytes())?;
    debug!("wrote jotfile to {}", path.display());
    Ok(())
}

/// Accepts a file path and converts it to a `Vec<Entry>` (rather than a
/// `Jotfile`).
pub fn read_from(path: &Path) -> Result<Jotfile> {
    let mut raw = String::new();
    let mut j = File::open(path)
        .chain_err(|| format!("no jotfile found at {}", path.display()))?;
    j.read_to_string(&mut raw)?;

    debug!("reading jotfile from {}", path.display());
    toml::from_str(&raw).chain_err(|| "failed to parse jotfile")
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_parse_date() {
        let content = "Testing a note! @2017-09-08";
        let p = date_from_content(content);
        assert_eq!(p.unwrap(), Some(Datetime::from_str("2017-09-08").unwrap()))
    }

    #[test]
    fn test_bad_date() {
        let content = "Testing a bad note! @2017-0908";
        assert!(date_from_content(content).is_err());
    }

    #[test]
    fn test_new_date_entry() {
        let content = "Testing a note! @2017-09-08";
        assert_eq!(
            Entry::new(content).unwrap(),
            Entry {
                content: String::from("Testing a note!"),
                done: false,
                ext: false,
                due: Some(Datetime::from_str("2017-09-08").unwrap()),
            }
        )
    }

    #[test]
    fn test_new_extended_entry() {
        let content = "Testing
a
note!";
        let e = Entry::new(content).unwrap();
        assert_eq!(
            e,
            Entry {
                content: String::from(content),
                done: false,
                ext: true,
                due: None,
            }
        )
    }

    #[test]
    fn test_date_on_newline_boundary() {
        let content = "Testing @2017-09-13
new
entry.";
        let e = Entry::new(content).unwrap();
        assert_eq!(e, Entry {
            content: String::from("Testing
new
entry."),
            done: false,
            ext: true,
            due: Some(Datetime::from_str("2017-09-13").unwrap()),
        })
    }
}
