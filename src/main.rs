#![recursion_limit = "128"]

extern crate chrono;
#[macro_use]
extern crate clap;
extern crate colored;
extern crate env_logger;
#[macro_use]
extern crate error_chain;
#[macro_use]
extern crate lazy_static;
#[macro_use]
extern crate log;
extern crate nix;
extern crate notify_rust;
extern crate regex;
#[macro_use]
extern crate serde_derive;
extern crate shellexpand;
extern crate tempfile;
extern crate toml;
extern crate xdg;

mod cli;
mod config;
mod daemon;
mod errors;
mod file;

use std::env;
use std::io::{self, Read, Seek, SeekFrom, Write};
use std::path::{Path, PathBuf};

use clap::ArgMatches;
use config::Config;
use errors::*;
use file::Entry;

fn run() -> Result<()> {
    env_logger::init()?;

    let matches = cli::cli().get_matches();

    match matches.occurrences_of("verbose") {
        1 => env::set_var("RUST_LOG", "info"),
        2 => env::set_var("RUST_LOG", "debug"),
        _ => env::set_var("RUST_LOG", "warn"),
    }

    let config = if matches.is_present("config") {
        debug!("using argument-provided config");
        Config::from_file(Path::new(matches.value_of("config").unwrap()))
    } else {
        debug!("using global config");
        config::find()
    }?;

    let jotpath = if matches.is_present("jotfile") {
        debug!("using argument-provided jotfile");
        PathBuf::from(matches.value_of("jotfile").unwrap())
    } else {
        debug!("using global jotfile");
        config.path().to_path_buf()
    };

    match matches.subcommand() {
        ("add", Some(subs)) => add_jot(&jotpath, subs),
        ("complete", Some(subs)) => comp_jot(&jotpath, subs),
        ("edit", Some(subs)) => edit_jot(&jotpath, subs),
        ("list", Some(subs)) => list_jots(&config, &jotpath, subs),
        ("remove", Some(subs)) => rm_jot(&jotpath, subs),
        ("clean", _) => clean_jots(&jotpath),
        ("daemon", Some(subs)) => daemon::handler(&jotpath, subs),
        _ => cli::cli().print_help().chain_err(|| "unable to print help"),
    }
}

/// Creates an instance of the environment `$EDITOR` to accept input from. May
/// be given text to pre-fill the editor with. Probably won't work on non-Unix.
fn read_from_editor(prefill: Option<&String>) -> Result<String> {
    let ed = env::var("EDITOR")?;
    info!("attempting to launch `{}`", ed);
    let mut tmp = tempfile::NamedTempFile::new()?;

    if let Some(content) = prefill {
        debug!("prefilling tempfile");
        write!(tmp, "{}", content)?;
    }

    std::process::Command::new(ed).arg(tmp.path()).status()?;

    debug!("reading from tempfile...");
    let mut buf = String::new();
    tmp.seek(SeekFrom::Start(0))?;
    tmp.read_to_string(&mut buf)?;
    debug!("... done");
    Ok(buf.trim().to_string())
}

fn add_jot(jp: &Path, m: &ArgMatches<'static>) -> Result<()> {
    let content: String = if m.is_present("extended") {
        debug!("reading from editor");
        read_from_editor(None)?
    } else {
        match m.value_of("msg") {
            Some(msg) => msg.to_owned(),
            None => {
                debug!("reading from stdin");
                let mut buf = String::new();
                print!("Jot: ");
                io::stdout().flush()?;
                io::stdin().read_line(&mut buf)?;
                buf
            }
        }
    };

    let e = Entry::new(&content)?;
    file::write_entry(jp, e)?;
    println!("[+] jot added");
    Ok(())
}

fn comp_jot(jp: &Path, m: &ArgMatches<'static>) -> Result<()> {
    let mut jots = file::read_from(jp)?.entry;
    let idx: usize = m.value_of("number").unwrap().parse()?;

    if !jots[idx - 1].done {
        jots[idx - 1].done = true;
        file::write_all(jp, &file::Jotfile { entry: jots })?;
        println!("[!] jot {} completed", idx);
    } else {
        println!("[?] jot {} already completed", idx);
    }
    Ok(())
}

fn edit_jot(jp: &Path, m: &ArgMatches<'static>) -> Result<()> {
    let mut jots = file::read_from(jp)?.entry;
    let idx: usize = m.value_of("number").unwrap().parse()?;
    let j = jots[idx - 1].clone();

    let newc = if m.is_present("extended") || j.ext {
        debug!("Reading from editor");
        read_from_editor(Some(&j.content))?
    } else {
        debug!("Reading from stdin");
        let mut buf = String::new();
        println!("Previously: {}", j.content);
        print!("Jot: ");
        io::stdout().flush()?;
        io::stdin().read_line(&mut buf)?;
        buf
    };

    jots[idx - 1] = Entry::new(&newc)?;
    jots[idx - 1].done = j.done; // preserve done state
    if j.due.is_some() && jots[idx - 1].due.is_none() {
        debug!("preserving due date");
        jots[idx - 1].due = j.due;
    }
    file::write_all(jp, &file::Jotfile { entry: jots })?;
    println!("[!] jot {} updated", idx);
    Ok(())
}


fn list_jots(c: &Config, jp: &Path, m: &ArgMatches<'static>) -> Result<()> {
    let jots = file::read_from(jp)?.entry;

    if !m.is_present("number") {
        for (i, j) in jots.iter().enumerate() {
            if j.done == m.is_present("done") || m.is_present("all") {
                println!(
                    "{}",
                    j.display_with(c, i + 1, m.is_present("extended"))
                );
            }
        }
    } else {
        let n: usize = m.value_of("number").unwrap().parse()?;
        println!("{}", jots[n - 1].display_with(c, n, true));
    }

    Ok(())
}

macro_rules! loop_input {
    ($yes:block, $no: block) => (loop {
        let mut buf = String::new();
        io::stdin().read_line(&mut buf)?;
        match buf.trim() {
            "n" | "no" | "N" | "NO" => { $no },
            "y" | "yes" | "Y" | "YES" => { $yes }
            _ => {
                print!("[!] Please answer Y/N: ");
                io::stdout().flush()?;
                continue;
            }
        }
    });
}

fn rm_jot(jp: &Path, m: &ArgMatches<'static>) -> Result<()> {
    let idx: usize = m.value_of("number").unwrap().parse()?;
    let mut jots = file::read_from(jp)?.entry;
    print!("[!] Really remove jot {}: {}? ", idx, jots[idx - 1]);
    io::stdout().flush()?;

    loop_input!(
        {
            let rm = jots.remove(idx - 1);
            debug!("Removed {:?}", rm);
            file::write_all(jp, &file::Jotfile { entry: jots })?;
            println!("[!] jot {} removed", idx);
            return Ok(());
        },
        { return Ok(()) }
    )
}

fn clean_jots(jp: &Path) -> Result<()> {
    println!("[!] This will remove all completed entries from your jotfile.");
    print!("[!] Are you sure you want to continue? ");
    io::stdout().flush()?;
    loop_input!(
        {
            debug!("Cleaning all completed entries");

            let jots = file::read_from(jp)?.entry;
            let mut newj = Vec::new();
            for j in jots {
                if !j.done {
                    newj.push(j);
                } else {
                    debug!("Removing {}", j);
                }
            }
            file::write_all(jp, &file::Jotfile { entry: newj })?;
            println!("[!] jots cleaned");
            return Ok(());
        },
        { return Ok(()) }
    )
}

#[cfg(unix)]
quick_main!(run);
