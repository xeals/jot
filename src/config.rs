use std::fmt;
use std::fs::File;
use std::io::Read;
use std::path::{Path, PathBuf};

use errors::*;
use shellexpand;
use toml;
use xdg;

lazy_static! {
    static ref XDG_DIRS: xdg::BaseDirectories =
        xdg::BaseDirectories::with_prefix("jot")
        .chain_err(|| "couldn't find XDG base directory").unwrap();
}

#[derive(Deserialize)]
struct RawConfig {
    jot_path: Option<String>,
    output: Option<RawOutput>,
}

#[derive(Deserialize)]
struct RawOutput {
    format: Option<String>,
    date_format: Option<String>,
    emph_title: Option<bool>,
    padding: Option<i64>,
    indent: Option<i64>,
    ext: Option<String>,
}

#[derive(PartialEq)]
pub struct Config {
    jot_path: String,
    pub output: Output,
}

#[derive(PartialEq)]
pub struct Output {
    format: String,
    date_format: String,
    pub emph_title: bool,
    pub padding: u8,
    pub indent: u8,
    pub ext: String,
}

impl Config {
    pub fn default() -> Config {
        Config {
            jot_path: String::from("~/.config/jot/jotfile"),
            output: Output {
                format: String::from("[{i}] {c}"),
                date_format: String::from("[{i}] {c} ({d})"),
                emph_title: true,
                padding: 2,
                indent: 4,
                ext: String::from("..."),
            },
        }
    }

    /// Returns a parsed config from the provided path. Will `Err` on a
    /// missing file (`IoError`) or failed parse (`TomlDeError`).
    pub fn from_file(path: &Path) -> Result<Config> {
        debug!("reading config file from {}", path.display());
        let mut buf = String::new();
        let mut config_file = File::open(path).chain_err(
            || "provided config is either a directory or doesn't exist",
        )?;
        config_file.read_to_string(&mut buf)?;
        parse(toml::from_str(&buf)?)
    }

    pub fn path(&self) -> PathBuf {
        PathBuf::from(shellexpand::tilde(&self.jot_path).into_owned())
    }

    pub fn formats(&self) -> (&String, &String) {
        (&(self.output.format), &(self.output.date_format))
    }
}

impl fmt::Display for Config {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(
            f,
            "Path: {}; format: `{}` / `{}`; padding: {}; indent: {}; ext: {}",
            self.jot_path,
            self.output.format,
            self.output.date_format,
            self.output.padding,
            self.output.indent,
            self.output.ext
        )
    }
}

/// Returns a config using either the user config (located in
/// `$XDG_CONFIG_HOME`) or the default config. Missing keys in a user config are
/// filled using the default.
pub fn find() -> Result<Config> {
    debug!("looking for \"default\" config file");
    match XDG_DIRS.find_config_file("config.toml") {
        Some(p) => Config::from_file(&p),
        None => Ok(Config::default()),
    }
}

macro_rules! sub_output {
    // for strings
    ($par:ident, $var:ident) => (match $par.$var {
        Some(x) => x,
        None => Config::default().output.$var,
    });
    // specific types
    ($par:ident, $var:ident, $fn:ty) => (match $par.$var {
        Some(x) => x as $fn,
        None => Config::default().output.$var,
    });}

/// Translates a `RawConfig` into a `Config`, substituting default values where
/// not defined.
fn parse(c: RawConfig) -> Result<Config> {
    Ok(Config {
        jot_path: match c.jot_path {
            Some(p) => p,
            None => Config::default().jot_path,
        },
        output: match c.output {
            Some(o) => Output {
                format: sub_output!(o, format),
                date_format: sub_output!(o, date_format),
                emph_title: sub_output!(o, emph_title),
                ext: sub_output!(o, ext),
                padding: sub_output!(o, padding, u8),
                indent: sub_output!(o, indent, u8),
            },
            None => Config::default().output,
        },
    })
}
