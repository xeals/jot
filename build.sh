#!/bin/sh -e

if command -v cargo >/dev/null ; then
    cargo build --release
else
    echo "You need cargo to build this project!"
    exit 1
fi

case "$1" in
    local)
        P=$HOME/.local/bin
        echo "Installing to $P"
        ;;
    global)
        P=/usr/local/bin
        S=sudo
        echo "Installing to $P"
        ;;
    *)
        P=$HOME/.local/bin
        echo "Installing to $P"
        ;;
esac

$S mkdir -p $P
$S cp target/release/jot $P
