# jot

`jot` is a command line tool to set and view reminders and notes.

## Building and Installation

Being written in Rust, building is managed through Rust's build tool, `cargo`. If Rust isn't in your distro's repos, you can get it [here](https://github.com/rust-lang/rust).

Building is done with

```
$ cargo build --release
```

The binary file will be in `target/release/jot`. Copy this to somewhere in your path (like `~/.local/bin`).

## Usage

All of jot's usage can be found in its help command:

```
$ jot help
```

### basic

Adding a new jot is as simple as

```
$ jot add -m "Take out the trash"
 [+] jot added
```

Your jots can be viewed with

```
$ jot list
 [1] Take out the trash
```

More complicated jots can be added with `add -x`. This will open whatever is defined as your `$EDITOR` (`nano` by default on many systems) for more detailed editing.

```
$ jot add -x
```

When you're done, save the file and it will be added as a new jot.

Jots with new lines in them will be truncated by default for easier viewing with `list`:

```
$ jot list
 [1] Take out the trash
 [2] See more ...
```

These can be view in full with `list -x` or by giving a single jot to view (e.g., `list 2`):

```
$ jot list -x
 [1] Take out the trash
 [2] See more
    Nice and easy!

$ jot list 2
 [2] See more
    Nice and easy!
```

### editing

Jots can be edited with `edit n`, where `n` is the jot to edit. If the jot has new lines in them, it will be opened in an editor; otherwise, you can rewrite it on the command line.

```
$ jot edit 1
Previously: Take out the trash

$ jot edit 2
# opens an editor
```

You can open a jot in an editor regardless by using `edit -x`.

### to-do tracking

Courtesy of the TOML format, `jot` offers dates. A jot with an associated date can be added with an `@` prefixed to the date in [RFC 3339](http://tools.ietf.org/html/rfc3339) format:

```
$ jot add -m "Should probably do my tax return @2017-10-01"
 [+] jot added

$ jot list
 [1] Take out the trash
 [2] See more ...
 [3] Should probably do my tax return (2017-10-01)
```

Currently the date remains in the jot text; this is planned to be changed in a future update. Possibly also on the map is customising the date output format.

Jots can be marked as done to take them off the list of to-dos:

```
$ jot comp 2
 [!] jot 2 completed

$ jot list
 [1] Take out the trash
 [3] Should probably do my tax return (2017-10-01)
```

You can view all your completed jots with `list -d`, or all your jots (completed or not) with `jot -a`.

### WIP: reminder daemon

`jot` has a very rudimentary reminder daemon that will send a native notification (via `libnotify`) the day before a jot is due. The daemon can be started with:

```
$ jot daemon start
```

### configuring
`jot` takes its configuration file from a file named `config.toml` sourced from, in order:

- a provided file with `jot -c $CONFIG_FILE`;
- the user configuration file in `$XDG_CONFIG_HOME` (by default, `~/.config`).

Based on the configuration file, it then takes the `jotfile` (where your jots are stored) from, in order:

- a provided file with `jot -j $JOTFILE`;
- a jotfile given in the `config.toml` file;
- the default jotfile location in `$XDG_CONFIG_HOME`.

See the default [`config.toml`](resources/config.toml) for examples on configuring. Any undefined options will be given their defaults. The configuration must be valid [TOML](https://github.com/toml-lang/toml), with the same types as the default.

## License

`jot` is released under the GNU General Public License, version 3.0. Information on this is in the [`LICENSE`](LICENSE) file.
